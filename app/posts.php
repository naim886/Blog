<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    protected  $fillable=['title', 'body', 'image', 'category_id', 'user_id'];

    public function category()
    {
        return $this->belongsTo('App\categories');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
