<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    protected  $fillable =['title', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function posts()
    {
        return $this->hasMany('App\posts');
    }

}
