<?php

namespace App\Http\Controllers;

use App\categories;
use App\posts;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;

class postsController extends Controller
{
    const UPLOAD_DIR = '/front/images/post_image/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=posts::latest()->paginate(15);
        $category=categories::pluck('title', 'id');
        return view('dashboard/posts/index', compact('posts','category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=categories::pluck('title', 'id');
        return view('dashboard/posts/create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $data['image'] = $this->uploadImage($file);
        } else {
            $data['image'] = null;
        }

        $data['user_id'] = auth()->user()->id;

        $post = posts::create($data);

        //$tag_ids = $request->tag_ids;

       // $post->tags()->attach($tag_ids);

        session()->flash('status', 'Post Insert successful!');

        return redirect('admin/post/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post= posts::findOrFail($id);
        return view('dashboard/posts/show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post =posts::findOrFail($id);
           // ->where('id', $id)
           // ->first();

        $this->unlinkImage($post->image);



        $post->forceDelete();

        session()->flash('status', 'Post Delete successful!');
        return redirect()->route('post.index');

    }

    private function uploadImage($file)
    {

        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $image_file_name = $timestamp . '.' . $file->getClientOriginalExtension();
        Image::make($file)->resize(830, 354)->save(public_path() . self::UPLOAD_DIR . $image_file_name);
        //$file->move(public_path() . self::UPLOAD_DIR, $image_file_name);
        return $image_file_name;
    }

    private function unlinkImage($img)
    {
        if ($img != '' && file_exists(public_path() . self::UPLOAD_DIR . $img)) {
            @unlink(public_path() . self::UPLOAD_DIR . $img);
        }
    }
}
