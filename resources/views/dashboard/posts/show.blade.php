@extends('dashboard.layouts.master')
@section('page_title' , 'Post Details | Home')
@section('breadcrumb', 'Post Details')
@section('content')
@section('headline', 'Post Details')
@section('content')

    <a href="{{route('post.index')}}"><button class="btn btn-outline-info">Post List</button></a> <br><br>

    <h3 class="text-center text-success">Title: {{$post->title}} Details </h3>
    <table class="table-hover" style="margin: 0 auto" cellspacing="20" cellpadding="10">

        <tbody>
        <tr>
            <td><strong>ID</strong></td>
            <td>: {{$post->id}}</td>
        </tr>
        <tr>
            <td><strong>Title</strong></td>
            <td>: {{$post->title}}</td>
        </tr>
        <tr>
            <td><strong>Post</strong></td>
            <td><p style="text-align: justify">: {{$post->body}}</p> </td>
        </tr>
        <tr>
            <td><strong>Created at</strong></td>
            <td>: {{$post->created_at}}</td>
        </tr>
        <tr>
            <td><strong>Created </strong></td>
            <td>: {{$post->created_at->diffForHumans()}}</td>
        </tr>
        <tr>
            <td><strong>Updated at</strong></td>
            <td>: {{$post->updated_at}}</td>
        </tr>
        <tr>
            <td><strong>Updated</strong> </td>
            <td>: {{$post->updated_at->diffForHumans()}}</td>
        </tr>
        <tr>
            <td><strong>Created By</strong> </td>
            <td>: {{$post->User->name}}</td>
        </tr>
        <tr>
            <td><strong>Image</strong> </td>
            <td>: <img src="{{asset('front/images/post_image')}}/{{$post->image}}"></td>
        </tr>
        </tbody>
    </table>

@endsection
