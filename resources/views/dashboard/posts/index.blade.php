@extends('dashboard.layouts.master')
@section('page_title' , 'Categories | Home')
@section('breadcrumb', 'Category List')
@section('content')
@section('headline', 'Category List')
@section('content')
    @if(session()->has('status'))
        <p class="text-center text-success">{{session('status')}}</p>
    @endif
   <strong>Select By Category :</strong>
    {!! Form::open(['url'=>'admin/post/', 'method'=>'post', 'files'=>true, 'class'=>'form-group' ]) !!}
    {!! Form::select('category_id', $category, ['class' => 'form-control', 'required'=>'required']) !!}
    {!! Form::button('Search', ['role'=>'button', 'class'=>'btn btn-info']) !!}
    {!! Form::close() !!}
    <br><br>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Serial</th>
            <th>Title</th>
            <th>Created By</th>
            <th>Created at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $sl=0;
        @endphp
        @foreach($posts as $post)
            <tr>
                <th scope="row">{{++$sl}}</th>
                <td width="30%"><a href="{{route('post.show',$post->id)}}" >{{$post->title}}</a></td>
                <td>{{$post->User->name}}</td>
                <td>{{$post->created_at->diffForHumans()}}</td>
                <td>

                    <a href="{{route('post.show',$post->id)}}"> <button class="btn btn-success">Show</button></a>

                    <a href="{{route('category.edit',$post->id)}}">  <button class="btn btn-info">Edit </button></a>

                    {!! Form::open(['url' => 'admin/post/'.$post->id, 'method'=>'delete', 'style' => 'display:inline' ])!!}

                    {!! Form::button('Delete', ['type'=>'submit', 'class'=> 'btn btn-danger' , 'onClick'=>"return confirm('Are you sure want to delete  ?')"]) !!}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach


        </tbody>
    </table>

    <a href="{{route('post.create')}}" > <button class="btn btn-outline-info">Create Post</button></a>



@endsection
