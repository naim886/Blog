@extends('dashboard.layouts.master')
@section('page_title' , 'Categories | Home')
@section('breadcrumb', 'New Post')
@section('content')
@section('headline', 'New Post')
@section('content')

    <a href="{{Route('post.index')}}"><button class="btn btn-success">Post List</button></a>

    <br><br>
    {!! Form::open(['url'=>'admin/post', 'method'=>'post', 'files'=>true, 'class'=>'form-group' ]) !!}

    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder'=>'Insert Post Title', 'required'=>'required']) !!}

    <br>
    {!! Form::textarea('body', null, ['class' => 'form-control', 'placeholder'=>'Write Your Post Here', 'required'=>'required']) !!}
    <br>
    {!! Form::label('category_id', 'Select Category',['class'=>'form-control']) !!}
    {!! Form::select('category_id', $category, ['class' => 'form-control', 'required'=>'required']) !!}
    <br><br>
    {!! Form::file('image', ['class' => 'form-control', 'required'=>'required','accept'=>'image/*']) !!}
    @if ($errors->has('image'))
        <span class="help-block">
            <strong>{{ $errors->first('image') }}</strong>
        </span>
    @endif
    <br>

    {!! Form::button('Add Post',['type'=>'submit', 'class'=>'btn btn-info']) !!}
    {!! Form::close() !!}
    <script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'body' );
    </script>

    <div class="clearfix"></div><br>

    @push('scripts')
        <script !src="">
            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#file').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function() {
                readURL(this);
            });
        </script>
    @endpush

@endsection



