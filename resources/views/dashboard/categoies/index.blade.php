@extends('dashboard.layouts.master')
@section('page_title' , 'Categories | Home')
@section('breadcrumb', 'Category List')
@section('content')
@section('headline', 'Category List')
@section('content')
    @if(session()->has('status'))
        <p class="text-center text-success">{{session('status')}}</p>
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Serial</th>
            <th>Title</th>
            <th>Created By</th>
            <th>Created at</th>
            <th>Updated at</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $sl=0;
        @endphp
@foreach($categories as $category)
        <tr>
            <th scope="row">{{++$sl}}</th>
            <td>{{$category->title}}</td>
            <td>{{$category->User->name}}</td>
            <td>{{$category->created_at->diffForHumans()}}</td>
            <td>{{$category->updated_at->diffForHumans()}}</td>
            <td>

                <a href="{{route('category.show',$category->id)}}"> <button class="btn btn-success">Show</button></a>

                <a href="{{route('category.edit',$category->id)}}">  <button class="btn btn-info">Edit </button></a>

                {!! Form::open(['url' => 'admin/category/'.$category->id, 'method'=>'delete', 'style' => 'display:inline' ])!!}

                {!! Form::button('Delete', ['type'=>'submit', 'class'=> 'btn btn-danger' , 'onClick'=>"return confirm('Are you sure want to delete  ?')"]) !!}

                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach


        </tbody>
    </table>

        <a href="{{route('category.create')}}" > <button class="btn btn-outline-info">Create Category</button></a>



@endsection
