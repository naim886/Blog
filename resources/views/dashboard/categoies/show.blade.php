@extends('dashboard.layouts.master')
@section('page_title' , 'Category Details | Home')
@section('breadcrumb', 'Category Details')
@section('content')
@section('headline', 'Category Details')
@section('content')

    <a href="{{route('category.index')}}"><button class="btn btn-outline-info">Category List</button></a>

<h3 class="text-center text-success">{{$category->title}} Details </h3>
<table class="table-hover" style="margin: 0 auto" cellspacing="20" cellpadding="10">

    <tbody>
    <tr>
        <td><strong>ID</strong></td>
        <td>: {{$category->id}}</td>
    </tr>
    <tr>
        <td><strong>Title</strong></td>
        <td>: {{$category->title}}</td>
    </tr>
    <tr>
        <td><strong>Created at</strong></td>
        <td>: {{$category->created_at}}</td>
    </tr>
    <tr>
        <td><strong>Created </strong></td>
        <td>: {{$category->created_at->diffForHumans()}}</td>
    </tr>
    <tr>
        <td><strong>Updated at</strong></td>
        <td>: {{$category->updated_at}}</td>
    </tr>
    <tr>
        <td><strong>Updated</strong> </td>
        <td>: {{$category->updated_at->diffForHumans()}}</td>
    </tr>
    <tr>
        <td><strong>Created By</strong> </td>
        <td>: {{$category->User->name}}</td>
    </tr>
    </tbody>
</table>

@endsection
