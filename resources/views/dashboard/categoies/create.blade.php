@extends('dashboard.layouts.master')
@section('page_title' , 'Categories | Home')
@section('breadcrumb', 'Add Category')
@section('content')
@section('headline', 'Add Category')
@section('content')

    <a href="{{Route('category.index')}}"><button class="btn btn-default"> Category List</button></a>
    <br><br>
    {!! Form::open(['url'=>'admin/category', 'method'=>'post',]) !!}

    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder'=>'Insert Category Name']) !!}
    <br>

    {!! Form::button('Add Category',['type'=>'submit', 'class'=>'btn btn-info']) !!}
    {!! Form::close() !!}


@endsection