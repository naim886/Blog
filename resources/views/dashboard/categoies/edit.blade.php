@extends('dashboard.layouts.master')
@section('page_title' , 'Categories | Home')
@section('breadcrumb', 'Add Category')
@section('content')
@section('headline', 'Update Category')
@section('content')

    <a href="{{Route('category.index')}}"><button class="btn btn-success">Category List</button></a>

    <br><br>
    {!! Form::open(['url'=>'admin/category/'.$category->id, 'method'=>'put',]) !!}
    {!! Form::text('title', $category->title, ['class' => 'form-control', 'placeholder'=>'Insert Category Name', 'required'=>'required']) !!}
    <br>

    {!! Form::button('Update Category',['type'=>'submit', 'class'=>'btn btn-info']) !!}
    {!! Form::close() !!}


@endsection