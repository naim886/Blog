@extends('students.layouts.master')
@section('page_title' , 'Crud Home')
@section('breadcrumb', 'Student List')
@section('content')

    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>{{$student->id}}</th>
        </tr>
        <tr>
            <th>First Name</th>
            <th>{{$student->fname}}</th>
        </tr>
        <tr>
            <th>Last Name</th>
            <th>{{$student->lname}}</th>
        </tr>
        <tr>
            <th>Created at </th>
            <th>{{$student->created_at}}</th>
        </tr>
        <tr>
            <th>Created Before </th>
            <th>{{$student->created_at->diffForHumans()}}</th>
        </tr>
        </thead>
        <tbody>
    </table>
    <button><a href="{{route('students.index')}}" >Student List</a> </button>



@endsection
