@extends('students.layouts.master')
@section('page_title' , 'Add New Student')
@section('breadcrumb', 'Add Student')
@section('content')

    <button><a href="{{Route('students.index')}}">List View</a> </button>
<h1 class="text-center">Add New Student</h1>
    students
    {!! Form::open(['url'=>'students', 'method'=>'post',]) !!}

    {!! Form::text('fname', null, ['class' => 'form-control', 'placeholder'=>'Insert Your First Name']) !!}
    {!! Form::text('lname', null, ['class' => 'form-control', 'placeholder'=>'Insert Your Last Name']) !!}
    {!! Form::button('Add Student',['type'=>'submit']) !!}
    {!! Form::close() !!}


@endsection