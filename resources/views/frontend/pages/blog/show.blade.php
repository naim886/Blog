@extends('frontend.layouts.blog')
@section('page_title','Blog')
@section('content')




    <div class="container">
        <div class="content-top">
             <div class="single">


                <div class="single-top">


                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('blog.index')}}">blog</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Post</li>
                        </ol>
                    </nav>

                    <h2><a href="{{route('blog.show',$post->id)}}">{{$post->title}}</a></h2>
                    <h6>Posted on :  {{$post->created_at->toDayDateTimeString()}} | By : <a href="#"> {{$post->User->name}}</a> | Category : {{$post->category->title}}</h6>

                    <img src="{{asset('front/images/post_image')}}/{{$post->image}}" title="{{$post->title}}">

                    <p class="sin" style="text-align: justify">{!!html_entity_decode($post->body)!!}} </p>
                    <div class="artical-links">
                        <ul>

                                <li><small> </small><span>{{$post->created_at->toDayDateTimeString()}}</span></li>
                                <li><a href="#"><small class="admin"> </small><span>{{$post->User->name}}</span></a></li>
                                <li><a href="#"><small class="no"> </small><span>{{DB::table('posts')->count()}} comments</span></a></li>
                                <li><a href="#"><small class="posts"> </small><span>{{DB::table('posts')->count()*5}} Views</span></a></li>


                        </ul>
                    </div>
                    <div class="respon">
                        <h2>Responses so far</h2>
                        <div class="strator">
                            <h5>ADMINISTRATOR</h5>
                            <p>feb 20th, 2015 at 9:41 pm</p>
                            <div class="strator-left">
                                <img src="images/co.png" class="img-responsive" alt="">
                            </div>
                            <div class="strator-right">
                                <p class="sin">Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra aug
                                    metus auctor fringilla. Donec id elit non mi porta  da at eget me  us, tellus ac
                                    ortor mauris ntum nibh, ut fermentum massa risus. Sed posuere consectetur
                                    Nulla vitae elit liber. Sed posuere consectetur est at lobortis.</p>
                            </div>
                            <div class="clearfix"></div>

                        </div>

                        <div class="comment">
                            <h2>Leave a comment</h2>
                            <form method="post" action="">
                                <input type="text" class="textbox" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
                                <input type="text" class="textbox" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
                                <input type="text" class="textbox" value="Website" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Website';}">
                                <textarea value="Message:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                                <div class="smt1">
                                    <input type="submit" value="add a comment">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


@endsection