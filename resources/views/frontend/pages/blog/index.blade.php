@extends('frontend.layouts.blog')
@section('page_title','Blog')
@section('content')

    <div class="container">
        <div class="blog">

            <div class="blog-content">
                <div class="blog-content-left">
                    <div class="blog-articals">



                        {{--post start from here--}}

                        <div class="blog-artical">
      @foreach($posts as $post)
                            <div class="blog-artical-info">
                                <div class="blog-artical-info-img">
                                    <a href="{{route('blog.show',$post->id)}}"><img src="{{asset('front/images/post_image')}}/{{$post->image}}" title="{{$post->title}}"></a>
                                </div>

                                <div class="blog-artical-info-head">
                                    <h2><a href="{{route('blog.show',$post->id)}}">{{$post->title}}</a></h2>
                                    <h6>Posted on :  {{$post->created_at->toDayDateTimeString()}} | By : <a href="#"> {{$post->User->name}}</a> | Category : {{$post->category->title}}</h6>

                                </div>
                                <div class="blog-artical-info-text">



                                    <p style="text-align: justify">{!!html_entity_decode($post->body)!!}}
                                    <a href="{{route('blog.show',$post->id)}}">[Read More]</a></p>

                                </div>

                                <div class="artical-links">
                                    <ul>
                                        <li><small> </small><span>{{$post->created_at->toDayDateTimeString()}}</span></li>
                                        <li><a href="#"><small class="admin"> </small><span>{{$post->User->name}}</span></a></li>
                                        <li><a href="#"><small class="no"> </small><span>{{DB::table('posts')->count()}} comments</span></a></li>
                                        <li><a href="#"><small class="posts"> </small><span>{{DB::table('posts')->count()*5}} Views</span></a></li>

                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                       {{--</div>--}}

                        @endforeach
                    {{--Post ends here--}}





                        {{--<div class="blog-artical">--}}

                            {{--<div class="blog-artical-info">--}}
                                {{--<div class="blog-artical-info-img">--}}
                                    {{--<a href="{{route('blog.show','1')}}"><img src="{{asset('front/images/8.jpg')}}" title="post-name"></a>--}}

                                {{--</div>--}}
                                {{--<div class="blog-artical-info-head">--}}
                                    {{--<h2><a href="{{route('blog.show','1')}}">Simply dummy text of the</a></h2>--}}
                                    {{--<h6>Posted on, 12 July 2014 at 10.30am by <a href="#"> admin</a></h6>--}}

                                {{--</div>--}}
                                {{--<div class="blog-artical-info-text">--}}
                                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy has been the industry's standard dummy has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<a href="#">[...]</a></p>--}}
                                {{--</div>--}}
                                {{--<div class="artical-links">--}}
                                    {{--<ul>--}}
                                        {{--<li><small> </small><span>june 14, 2013</span></li>--}}
                                        {{--<li><a href="#"><small class="admin"> </small><span>admin</span></a></li>--}}
                                        {{--<li><a href="#"><small class="no"> </small><span>No comments</span></a></li>--}}
                                        {{--<li><a href="#"><small class="posts"> </small><span>View posts</span></a></li>--}}
                                        {{--<li><a href="#"><small class="link"> </small><span>permalink</span></a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="clearfix"> </div>--}}
                        {{--</div>--}}
                        {{--<div class="blog-artical">--}}

                            {{--<div class="blog-artical-info">--}}
                                {{--<div class="blog-artical-info-img">--}}
                                    {{--<a href="{{route('blog.show','1')}}"><img src="{{asset('front/images/.jpg')}}" title="post-name"></a>--}}
                                {{--</div>--}}
                                {{--<div class="blog-artical-info-head">--}}
                                    {{--<h2><a href="{{route('blog.show','1')}}">Lorem Ipsum has been the</a></h2>--}}
                                    {{--<h6>Posted on, 12 July 2014 at 10.30am by <a href="#"> admin</a></h6>--}}

                                {{--</div>--}}
                                {{--<div class="blog-artical-info-text">--}}
                                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy has been the industry's standard dummy has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<a href="#">[...]</a></p>--}}
                                {{--</div>--}}
                                {{--<div class="artical-links">--}}
                                    {{--<ul>--}}
                                        {{--<li><small> </small><span>june 14, 2013</span></li>--}}
                                        {{--<li><a href="#"><small class="admin"> </small><span>admin</span></a></li>--}}
                                        {{--<li><a href="#"><small class="no"> </small><span>No comments</span></a></li>--}}
                                        {{--<li><a href="#"><small class="posts"> </small><span>View posts</span></a></li>--}}
                                        {{--<li><a href="#"><small class="link"> </small><span>permalink</span></a></li>--}}
                                    {{--</ul>--}}
                                    {{--</div>--}}
                            {{--</div>--}}
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <!--start-blog-pagenate-->
                    <nav>
                        {{ $posts->links() }}
                    </nav>
                    <!--//End-blog-pagenate-->


</div>

@endsection