@extends('frontend.layouts.master')
@section('page_title','Home')

@section('content')


<div class="nam-matis">
    @foreach($posts as $post)
    <div class="nam-matis-top">


        <div class="col-md-6 nam-matis-1">
            <a href="{{route('blog.show',$post->id)}}"><img src="{{asset('front/images/post_image')}}/{{$post->image}}" class="img-responsive" alt="{{$post->title}}"></a>
            <h3><a href="{{route('blog.show',$post->id)}}">{{$post->title}}</a></h3>
            <p>{{str_limit($post->body,200)}} </p>
        </div>
    </div>
@endforeach


        {{--<div class="col-md-6 nam-matis-1">--}}
            {{--<a href=""><img src="{{asset('front/images/6.jpg')}}" class="img-responsive" alt=""></a>--}}
            {{--<h3><a href="">Suspendisse a pellentesque dui</a></h3>--}}
            {{--<p>Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada                elit lectus felis, malesuada ultricies. Curabitur et ligula.</p>--}}
        {{--</div>--}}
        {{--<div class="clearfix"> </div>--}}
    {{--</div>--}}


    {{--<div class="nam-matis-top">--}}
        {{--<div class="col-md-6 nam-matis-1">--}}
            {{--<a href="{{route('blog.show','1')}}"><img src="{{asset('front/images/4.jpg')}}" class="img-responsive" alt=""></a>--}}
            {{--<h3><a href="{{route('blog.show','1')}}">Suspendisse a pellentesque dui</a></h3>--}}
            {{--<p>Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada                elit lectus felis, malesuada ultricies. Curabitur et ligula.</p>--}}
        {{--</div>--}}


        {{--<div class="col-md-6 nam-matis-1">--}}
            {{--<a href="{{route('blog.show','1')}}"><img src="{{asset('front/images/1.jpg')}}" class="img-responsive" alt=""></a>--}}
            {{--<h3><a href="{{route('blog.show','1')}}">Suspendisse a pellentesque dui</a></h3>--}}
            {{--<p>Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada                elit lectus felis, malesuada ultricies. Curabitur et ligula.</p>--}}
        {{--</div>--}}


        {{--<div class="clearfix"> </div>--}}
    {{--</div>--}}
</div>
<!-- nam-matis -->
</div>


@endsection