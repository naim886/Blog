@extends('frontend.layouts.fullwidth')
@section('page_title','Contact')
@section('fullwidth_content')

    <div class="container">
    <div class="contact">
        <div class="main-head-section">

            <h3>Contact</h3>

            <div class="contact-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d228.1897279427382!2d90.36005536632469!3d23.781744918895537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2588483a2e20a340!2sDhaka+Business+Point!5e0!3m2!1sen!2sbd!4v1521743744264" width="600" height="450" frameborder="0" style="border:1px solid silver" allowfullscreen></iframe>
            </div>
        </div>

        <div class="contact_top">

            <div class="col-md-8 contact_left">
                <h4>Contact Form</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt dolor et tristique bibendum. Aenean sollicitudin vitae dolor ut posuere.</p>
                <form>
                    <div class="form_details">
                        <input type="text" class="text" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
                        <input type="text" class="text" value="Email Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}">
                        <input type="text" class="text" value="Subject" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Subject';}">
                        <textarea value="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                        <div class="clearfix"> </div>
                        <div class="sub-button">
                            <input type="submit" value="Send message">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 company-right">
                <div class="company_ad">
                    <h3>Contact Info</h3>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit velit justo.</span>
                    <address>
                        <p>E-mail:<a href="#">naim886@gmail.com</a></p>
                        <p>Phone: 01711980213</p>
                        <p>54/A Kollyanpur Main Road</p>
                        <p>Kollaynpur, Dhaka - 1207</p>

                    </address>
                </div>

            </div>
            <div class="clearfix"> </div>

        </div>
    </div>
   @endsection