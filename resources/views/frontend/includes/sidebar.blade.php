
<div class="col-md-3 bann-left">
    <div class="b-search">
        <form>
            <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
            <input type="submit" value="">
        </form>
    </div>


    <h3>Recent Posts</h3>
    <div class="blo-top">
        @foreach($recents as $recent)
        <div class="blog-grids">
            <div class="blog-grid-left">
                <a href="{{route('show')}}"><img src="{{asset('front/images/post_image')}}/{{$recent->image}}" class="img-responsive" alt="" width="100px"></a>
            </div>
            <div class="blog-grid-right">
                <h4><a href="{{route('show')}}">{{$recent->title}} </a></h4>

            </div>
            <div class="clearfix"> </div>
        </div>
        @endforeach
        {{--<div class="blog-grids">--}}
            {{--<div class="blog-grid-left">--}}
                {{--<a href="{{route('show')}}"><img src="{{asset('front/images/2b.jpg')}}" class="img-responsive" alt=""></a>--}}
            {{--</div>--}}
            {{--<div class="blog-grid-right">--}}
                {{--<h4><a href="{{route('show')}}">Little Invaders </a></h4>--}}
                {{--<p>pellentesque dui, non felis. Maecenas male </p>--}}
            {{--</div>--}}
            {{--<div class="clearfix"> </div>--}}
        {{--</div>--}}
        {{--<div class="blog-grids">--}}
            {{--<div class="blog-grid-left">--}}
                {{--<a href=""><img src="{{asset('front/images/3b.jpg')}}" class="img-responsive" alt=""></a>--}}
            {{--</div>--}}
            {{--<div class="blog-grid-right">--}}
                {{--<h4><a href="{{route('show')}}">Little Invaders </a></h4>--}}
                {{--<p>pellentesque dui, non felis. Maecenas male </p>--}}
            {{--</div>--}}
            {{--<div class="clearfix"> </div>--}}
        {{--</div>--}}
    </div>
    <h3>Categories</h3>
    <div class="blo-top">
        @foreach($categories as $category)
        <li><a href="#">||   {{$category->title}}</a></li>
      @endforeach
    </div>
    <h3>Newsletter</h3>

    <div class="blo-top">
        <div class="name">
            <form>
                <input type="text" placeholder="email" required="">
            </form>
        </div>
        <div class="button">
            <form>
                <input type="submit" value="Subscribe">
            </form>
        </div>


        <div class="clearfix"></div>
        <div class="clearfix">
            <h3>Tags Weight</h3>
            <div class="b-tag-weight">
                 <ul>
                    <li><a href="#">Lorem</a></li>
                    <li><a href="#">consectetur</a></li>
                    <li><a href="#">dolore</a></li>
                    <li><a href="#">aliqua</a></li>
                    <li><a href="#">sit amet</a></li>
                    <li><a href="#">ipsum</a></li>
                    <li><a href="#">Lorem</a></li>
                    <li><a href="#">consectetur</a></li>
                    <li><a href="#">dolore</a></li>
                    <li><a href="#">aliqua</a></li>
                    <li><a href="#">sit amet</a></li>
                    <li><a href="#">ipsum</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>