-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2018 at 12:43 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lara_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `user_id`, `created_at`, `updated_at`) VALUES
(8, 'Programming', 1, '2018-04-08 14:57:39', '2018-04-08 14:57:39'),
(9, 'Graphics Design', 1, '2018-04-08 14:57:48', '2018-04-08 14:57:48'),
(10, 'Electronics', 1, '2018-04-08 14:57:57', '2018-04-08 14:57:57'),
(11, 'Tech News', 1, '2018-04-08 14:58:08', '2018-04-08 14:58:08'),
(12, 'Hacking', 1, '2018-04-08 14:58:14', '2018-04-08 14:58:14'),
(13, 'Electronics', 1, '2018-04-08 14:58:25', '2018-04-08 14:58:25'),
(14, 'Hardware', 1, '2018-04-08 14:58:54', '2018-04-08 14:58:54'),
(15, 'Software', 1, '2018-04-08 14:59:02', '2018-04-08 14:59:02'),
(16, 'Mobile', 1, '2018-04-08 14:59:23', '2018-04-08 14:59:23'),
(17, 'Computer', 1, '2018-04-08 14:59:30', '2018-04-08 14:59:30'),
(19, 'Laptop', 1, '2018-04-08 14:59:53', '2018-04-08 14:59:53');

-- --------------------------------------------------------

--
-- Table structure for table `commentables`
--

CREATE TABLE `commentables` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `commentable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentable_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `likeables`
--

CREATE TABLE `likeables` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_03_22_232641_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `image`, `user_id`, `category_id`, `created_at`, `updated_at`) VALUES
(7, 'Russia seeks to block Telegram messaging app', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tinci arted. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliq usit e adipiscing elit, sed diam nonummy nibh euismod tinci. Donec sed odio dui. Duis mollis, est non cons commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Amet, consectetuer diam teri adipiscing elit, sed diam nonummy nibh euismod tinci.</p>\r\n\r\n<p>Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue. Donec ullamco elitider per nulla non metus auctor fringilla. Donec id elit non mi porta gravi da at eget metus. Fusce dap gravi ibus, tellus ac cursus commodo, Nulla vitae elit libero, a torto ediri pharetra augue. Donec ullamco per nulla non metus auctor fringilla.</p>', '2018-04-08-21-03-02.jpg', 1, 16, '2018-04-08 15:03:02', '2018-04-08 15:03:02'),
(8, 'How the internet is clogging up city streets', '<p>Traffic in New York is slowing down. Jams are endemic in Manhattan, especially in its business districts. Daytime traffic in the busiest areas now moves almost 20% more slowly than it did five years ago.</p>\r\n\r\n<p>It seems a place ripe for wide use of ride-hailing apps that, you might think, would alleviate some of the congestion.</p>\r\n\r\n<p>Except, those apps appear to be making things worse as traffic has slowed in line with the growing popularity of apps such as Uber and Lyft, suggests&nbsp;<a href=\"http://schallerconsult.com/rideservices/emptyseats.pdf\">a study by transport expert Bruce Schaller.</a></p>\r\n\r\n<p>Over the four years of the study, the number of cars in Manhattan seeking ride-hailing fares increased by 81%. There are now about 68,000 ride-sharing drivers across New York city. That&#39;s about five times the number of distinctive yellow cabs licensed to operate there, he found. There are so many, his work suggests, that they spend about 45% of their time empty just cruising for fares. That is a lot of unused cars clogging a lot of busy streets.</p>\r\n\r\n<p>Simple physics explains why such a glut of ride-sharing vehicles is causing, not curing, congestion, said Jarrett Walker, a public transport policy expert who has advised hundreds of cities about moving people.</p>\r\n\r\n<p>&quot;Lots and lots of people are deciding that, &#39;Oh, public transport is just too much of a hassle this morning,&#39; or whenever, which causes a shift in patronage from public transport to ride-sharing services,&quot; he told the BBC.</p>\r\n\r\n<p>&quot;That means moving people from larger vehicles into smaller ones, which means more vehicles to move the same people.</p>\r\n\r\n<p>&quot;Therefore, more traffic.&quot;</p>', '2018-04-08-21-04-21.jpg', 1, 10, '2018-04-08 15:04:21', '2018-04-08 15:04:21'),
(9, 'Chinese space station burns up over South Pacific', '<p><strong>- Tiangong-1 space station re-enters atmosphere</strong></p>\r\n\r\n<p><strong>-- Space station was launched in 2011&nbsp;</strong></p>\r\n\r\n<p><strong>China&rsquo;s Tiangong-1 space station re-entered the earth&rsquo;s atmosphere and burnt up over the middle of the South Pacific today, the Chinese space authority said.</strong></p>\r\n\r\n<p>The &ldquo;vast majority&rdquo; of the craft burnt up on re-entry, at around 8:15am (0015 GMT), the authority said in a brief statement on its website, without saying exactly where the remnants might have landed.</p>\r\n\r\n<p>Earlier, it had said the craft was expected to re-enter the atmosphere off the Brazilian coast in the South Atlantic near the cities of Sao Paulo and Rio de Janeiro.</p>\r\n\r\n<p>The United States Air Force 18th Space Control Squadron, which tracks and detects all artificial objects in Earth&#39;s orbit, said it had also tracked the Tiangong-1 in its re-entry over the South Pacific.</p>\r\n\r\n<p>It said in a statement it had confirmed re-entry in coordination with counterparts in Australia, Canada, France, Germany, Italy, Japan, South Korea and Britain.</p>\r\n\r\n<p>The remnants of Tiangong-1 appeared to have landed about 100 km (62 miles) northwest of Tahiti, said Brad Tucker, an astrophysicist at Australian National University.</p>\r\n\r\n<p>&quot;Small bits definitely will have made it to the surface,&quot; he told Reuters, adding that while about 90 percent would have burnt up in the atmosphere and just 10 percent made it to the ground, that fraction still amounted to 700 kg (1,543 lb) to 800 kg (1,764 lb).</p>\r\n\r\n<p>&quot;Most likely the debris is in the ocean, and even if people stumbled over it, it would just look like rubbish in the ocean and be spread over a huge area of thousands of square kilometres.&quot;</p>\r\n\r\n<p>Beijing said on Friday it was unlikely any large pieces would reach the ground.</p>\r\n\r\n<p>The 10.4-metre-long (34.1-foot) Tiangong-1, or &quot;Heavenly Palace 1&quot;, was launched in 2011 to carry out docking and orbit experiments as part of China&#39;s ambitious space programme, which aims to place a permanent station in orbit by 2023. It was originally planned to be decommissioned in 2013 but its mission was repeatedly extended.</p>\r\n\r\n<p>China had said re-entry would happen in late 2017, but that process was delayed, leading some experts to suggest the space laboratory was out of control.Worldwide media hype about the re-entry reflected overseas &quot;envy&quot; of China&#39;s space industry, the Chinese tabloid Global Times said on Monday. &quot;It&#39;s normal for spacecraft to re-enter the atmosphere, yet Tiangong-1 received so much attention, partly because some Western countries are trying to hype and sling mud at China&#39;s fast-growing aerospace industry,&quot; it said.</p>', '2018-04-09-10-25-45.jpg', 1, 11, '2018-04-09 04:25:45', '2018-04-09 04:25:45'),
(10, 'Scientists dim sunlight, suck up carbon dioxide to cool planet', '<p><strong>cientists are sucking carbon dioxide from the air with giant fans and preparing to release chemicals from a balloon to dim the sun&#39;s rays as part of a climate engineering push to cool the planet.</strong></p>\r\n\r\n<p>Backers say the risky, often expensive projects are urgently needed to find ways of meeting the goals of the Paris climate deal to curb global warming that researchers blame for causing more heatwaves, downpours and rising sea levels.</p>\r\n\r\n<p>The United Nations says the targets are way off track and will not be met simply by reducing emissions for example from factories or cars - particularly after US President Donald Trump&#39;s decision to pull out of the 2015 pact.</p>\r\n\r\n<p>They are pushing for other ways to keep temperatures down.</p>\r\n\r\n<p>In the countryside near Zurich, Swiss company Climeworks began to suck greenhouse gases from thin air in May with giant fans and filters in a $23 million project that it calls the world&#39;s first &quot;commercial carbon dioxide capture plant&quot;.</p>\r\n\r\n<p>Worldwide, &quot;direct air capture&quot; research by a handful of companies such as Climeworks has gained tens of millions of dollars in recent years from sources including governments, Microsoft founder Bill Gates and the European Space Agency.</p>\r\n\r\n<p>If buried underground, vast amounts of greenhouse gases extracted from the air would help reduce global temperatures, a radical step beyond cuts in emissions that are the main focus of the Paris Agreement.</p>\r\n\r\n<p>Climeworks reckons it now costs about $600 to extract a tonne of carbon dioxide from the air and the plant&#39;s full capacity due by the end of 2017 is only 900 tonnes a year. That&#39;s equivalent to the annual emissions of only 45 Americans.</p>\r\n\r\n<p>And Climeworks sells the gas, at a loss, to nearby greenhouses as a fertilizer to grow tomatoes and cucumbers and has a partnership with carmaker Audi, which hopes to use carbon in greener fuels.</p>\r\n\r\n<p>Jan Wurzbacher, director and founder of Climeworks, says the company has planet-altering ambitions by cutting costs to about $100 a tonne and capturing one percent of global man-made carbon emissions a year by 2025.</p>\r\n\r\n<p>&quot;Since the Paris Agreement, the business substantially changed,&quot; he said, with a shift in investor and shareholder interest away from industrial uses of carbon to curbing climate change.</p>\r\n\r\n<p>But penalties for factories, power plants and cars to emit carbon dioxide into the atmosphere are low or non-existent. It costs 5 euros ($5.82) a tonne in the European Union.</p>\r\n\r\n<p>And isolating carbon dioxide is complex because the gas makes up just 0.04 percent of the air. Pure carbon dioxide delivered by trucks, for use in greenhouses or to make drinks fizzy, costs up to about $300 a tonne in Switzerland.</p>\r\n\r\n<p>Other companies involved in direct air capture include Carbon Engineering in Canada, Global Thermostat in the United States and Skytree in the Netherlands, a spinoff of the European Space Agency originally set up to find ways to filter out carbon dioxide breathed out by astronauts in spacecrafts.</p>\r\n\r\n<h2>Not Science Fiction</h2>\r\n\r\n<p>The Paris Agreement seeks to limit a rise in world temperatures this century to less than 2 degrees Celsius (3.6 Fahrenheit), ideally 1.5C (2.7F) above pre-industrial times.</p>\r\n\r\n<p>But UN data show that current plans for cuts in emissions will be insufficient, especially without the United States, and that the world will have to switch to net &quot;negative emissions&quot; this century by extracting carbon from nature.</p>\r\n\r\n<p>Riskier &quot;geo-engineering&quot; solutions could be a backstop, such as dimming the world&#39;s sunshine, dumping iron into the oceans to soak up carbon, or trying to create clouds.</p>\r\n\r\n<p>Among new university research, a Harvard geo-engineering project into dimming sunlight to cool the planet set up in 2016 has raised $7.5 million from private donors. It plans a first outdoor experiment in 2018 above Arizona.</p>\r\n\r\n<p>&quot;If you want to be confident to get to 1.5 degrees you need to have solar geo-engineering,&quot; said David Keith, of Harvard.</p>\r\n\r\n<p>Keith&#39;s team aims to release about 1 kilo (2.2 lbs) of sun dimming material, perhaps calcium carbonate, from a high-altitude balloon above Arizona next year in a tiny experiment to see how it affects the microphysics of the stratosphere.</p>\r\n\r\n<p>&quot;I don&#39;t think it&#39;s science fiction ... to me it&#39;s normal atmospheric science,&quot; he said.</p>\r\n\r\n<p>Some research has suggested that geo-engineering with sun-dimming chemicals, for instance, could affect global weather patterns and disrupt vital Monsoons.</p>\r\n\r\n<p>And many experts fear that pinning hopes on any technology to fix climate change is a distraction from cuts in emissions blamed for heating the planet.</p>\r\n\r\n<p>&quot;Relying on big future deployments of carbon removal technologies is like eating lots of dessert today, with great hopes for liposuction tomorrow,&quot; Christopher Field, a Stanford University professor of climate change, wrote in May.</p>\r\n\r\n<p>Jim Thomas of ETC Group in Canada, which opposes climate engineering, said direct air capture could create &quot;the illusion of a fix that can be used cynically or naively to entertain policy ideas such as &#39;overshoot&#39;&quot; of the Paris goals.</p>\r\n\r\n<p>But governments face a dilemma. Average surface temperatures are already about 1C (1.8F) above pre-industrial levels and hit record highs last year.</p>\r\n\r\n<p>&quot;We&#39;re in trouble,&quot; said Janos Pasztor, head of the new Carnegie Climate Geoengineering Governance Project. &quot;The question is not whether or not there will be an overshoot but by how many degrees and for how many decades.&quot;</p>\r\n\r\n<p>Faced with hard choices, many experts say that extracting carbon from the atmosphere is among the less risky options. Leaders of major economies, except Trump, said at a summit in Germany this month that the Paris accord was &quot;irreversible.&quot;</p>\r\n\r\n<h2>Barking Mad</h2>\r\n\r\n<p>Raymond Pierrehumbert, a professor of physics at Oxford University, said solar geo-engineering projects seemed &quot;barking mad&quot;.</p>\r\n\r\n<p>By contrast, he said &quot;carbon dioxide removal is challenging technologically, but deserves investment and trial.&quot;</p>\r\n\r\n<p>The most natural way to extract carbon from the air is to plant forests that absorb the gas as they grow, but that would divert vast tracts of land from farming. Another option is to build power plants that burn wood and bury the carbon dioxide released.</p>\r\n\r\n<p>Carbon Engineering, set up in 2009 with support from Gates and Murray Edwards, chairman of oil and gas group Canadian Natural Resources Ltd, has raised about $40 million and extracts about a tonne of carbon dioxide a day with turbines and filters.</p>\r\n\r\n<p>&quot;We&#39;re mainly looking to synthesize fuels&quot; for markets such as California with high carbon prices, said Geoffrey Holmes, business development manager at Carbon Engineering.</p>\r\n\r\n<p>But he added that &quot;the Paris Agreement helps&quot; with longer-term options of sucking large amounts from the air.</p>\r\n\r\n<p>Among other possible geo-engineering techniques are to create clouds that reflect sunlight back into space, perhaps by using a mist of sea spray.</p>\r\n\r\n<p>That might be used locally, for instance, to protect the Great Barrier Reef in Australia, said Kelly Wanser, principal director of the US-based Marine Cloud Brightening Project.</p>\r\n\r\n<p>Among new ideas, Wurzbacher at Climeworks is sounding out investors on what he says is the first offer to capture and bury 50 tonnes of carbon dioxide from the air, for $500 a tonne.</p>\r\n\r\n<p>That might appeal to a company wanting to be on forefront of a new green technology, he said, even though it makes no apparent economic sense.</p>', '2018-04-09-10-27-23.jpg', 1, 11, '2018-04-09 04:27:23', '2018-04-09 04:27:23');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tagables`
--

CREATE TABLE `tagables` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `tagable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagable_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Md. Naimul Hasan', 'naim886@gmail.com', '$2y$10$drqkGvDsAkhy4AuD3.938.kqECFE.yHMsCwZRpGIzFtBIJcN54kce', 'CulhuCryU83zVr1ZMgADoK6Cj4O1wueA782Tz4PuaEvttNF3VlWZsdsiG98G', '2018-03-23 06:25:02', '2018-03-23 06:25:02'),
(2, 'Md. Ashfaqur Rahman', 'ashfak@gmail.com', '$2y$10$EBTtToUmUZzpTDbMuCPLtela.TMbiRa2lODtGx9E/42lctdncqf9K', 'WqmPlAxza32rbJm2PW0IGBxBb1rrZICidWOhOSsR0Syt7mYIKuKFVDByv2pb', '2018-03-28 12:48:25', '2018-03-28 12:48:25'),
(3, 'Mousumi Dewan', 'mou@gmail.com', '$2y$10$nC4K8IH5VAWu9YjgG/CAuujlN5xD61hApnDAnU5LjmGE5SA9ZmZZS', 'BDqhCDgRjvRzWcB0mRdKwidzZTCWrcCVj2Y83c0vhpGQ606hqKR1EEjcpufv', '2018-04-04 14:41:59', '2018-04-04 14:41:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_user_id_foreign` (`user_id`);

--
-- Indexes for table `commentables`
--
ALTER TABLE `commentables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_commentable_type_commentable_id_index` (`commentable_type`,`commentable_id`);

--
-- Indexes for table `likeables`
--
ALTER TABLE `likeables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_foreign` (`user_id`);

--
-- Indexes for table `tagables`
--
ALTER TABLE `tagables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagables_tagable_type_tagable_id_index` (`tagable_type`,`tagable_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `commentables`
--
ALTER TABLE `commentables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `likeables`
--
ALTER TABLE `likeables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tagables`
--
ALTER TABLE `tagables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
